# Rest API
请务必设置user agent为 'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36'

如果在使用过程中有任何问题请联系我们API技术QQ群： 764113552，我们将在第一时间帮您解决技术问题。

# 示例代码
签名方式： 先用sha加密secretkey，然后根据加密过的secretkey把请求的参数签名，请求参数按照ascii值排序加密，通过md5填充16位加密

[github 示例代码](https://github.com/zb2017/api?_blank)

# 访问限制
1.单个IP限制每分钟1000次访问，超过1000次将被锁定1小时，一小时后自动解锁。

2.单个用户限制每秒钟30次访问，一秒钟内30次以上的请求，将会视作无效。

3.K线接口每秒只能请求一次数据。

