## 借款
```request
{
　　"accesskey":"your accesskey",
　　"amount":100,
　　"channel":"borrow",
　　"coin":"qc",
　　"event":"addChannel",
　　"interestRateOfDay":0.15
　　"isLoop":1
　　"repaymentDay":20
　　"safePwd":资金安全密码
　　"sign":签名
}
```
```response
{
　　"success":true,
　　"code":1000,
　　"channel":"borrow",
　　"message":"操作成功",
　　"no":"0"
}
```
**请求参数**

参数名称 | 类型 | 取值范围
---|---|---
accesskey | String | accesskey
channel | String | borrow
event | String | 直接赋值addChannel
marketName | String | 杠杆市场
coin | String | 币种
amount | Double | 借入金额
interestRateOfDay | Double | 日利率 [0.05-0.2]%
repaymentDay | int | 借款期限 [10/20/30]天
isLoop | int | 是否自动续借 [1/0](到期自动续借/到期自动还款)
no | String | 请求的唯一标识，用于在返回内容时区分
sign |String | 签名

```resdata
success : 是否成功
code : 返回代码
message : 提示信息
channel : 请求的频道
no : 请求的唯一标识，用于在返回内容时区分
```

```java
public void borrow() {
	//测试apiKey:ce2a18e0-dshs-4c44-4515-9aca67dd706e
	//测试secretKey:c11a122s-dshs-shsa-4515-954a67dd706e
	//加密类:https://github.com/zb2017/api/blob/master/zb_netty_client_java/src/main/java/websocketx/client/EncryDigestUtil.java
	//secretKey通过sha1加密得到:86429c69799d3d6ac5da5c2c514baa874d75a4ba
	String secret = EncryDigestUtil.digest("c11a122s-dshs-shsa-4515-954a67dd706e");
	//参数按照ASCII值排序
	String params = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","amount":"100","channel":"borrow","coin":"qc","event":"addChannel","interestRateOfDay":"0.15","isLoop":"1","no":"test001","repaymentDay":"20","safePwd":"123456"}";
	//sign通过HmacMD5加密得到:378348551685d6d5ddbdf02a4df71f43
	String sign = EncryDigestUtil.hmacSign(params, secret);
	//最终发送到服务器参数json请求
	String json = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","amount":"100","channel":"borrow","coin":"qc","event":"addChannel","interestRateOfDay":"0.15","isLoop":"1","no":"test001","repaymentDay":"20","safePwd":"123456","sign":"378348551685d6d5ddbdf02a4df71f43"}";
	ws.sendText(json);
}
```

```python
import hashlib
import zlib
import json
from time import sleep
from threading import Thread

import websocket    
import urllib2, hashlib,struct,sha,time


zb_usd_url = "wss://api.zb.cn:9999/websocket"

class ZB_Sub_Spot_Api(object):
    """基于Websocket的API对象"""
    def __init__(self):
        """Constructor"""
        self.apiKey = 'ce2a18e0-dshs-4c44-4515-9aca67dd706e'        
        self.secretKey = 'c11a122s-dshs-shsa-4515-954a67dd706e'     

        self.ws_sub_spot = None          # websocket应用对象  现货对象

    #----------------------------------------------------------------------
    def reconnect(self):
        """重新连接"""
        # 首先关闭之前的连接
        self.close()
        
        # 再执行重连任务
        self.ws_sub_spot = websocket.WebSocketApp(self.host, 
                                         on_message=self.onMessage,
                                         on_error=self.onError,
                                         on_close=self.onClose,
                                         on_open=self.onOpen)        
    
        self.thread = Thread(target=self.ws_sub_spot.run_forever)
        self.thread.start()
    
    #----------------------------------------------------------------------
    def connect_Subpot(self, apiKey , secretKey , trace = False):
        self.host = zb_usd_url
        self.apiKey = apiKey
        self.secretKey = secretKey

        websocket.enableTrace(trace)

        self.ws_sub_spot = websocket.WebSocketApp(self.host, 
                                             on_message=self.onMessage,
                                             on_error=self.onError,
                                             on_close=self.onClose,
                                             on_open=self.onOpen)        
            
        self.thread = Thread(target=self.ws_sub_spot.run_forever)
        self.thread.start()

    #----------------------------------------------------------------------
    def readData(self, evt):
        """解压缩推送收到的数据"""
        # # 创建解压器
        # decompress = zlib.decompressobj(-zlib.MAX_WBITS)
        
        # # 将原始数据解压成字符串
        # inflated = decompress.decompress(evt) + decompress.flush()
        
        # 通过json解析字符串
        data = json.loads(evt)
        
        return data

    #----------------------------------------------------------------------
    def close(self):
        """关闭接口"""
        if self.thread and self.thread.isAlive():
            self.ws_sub_spot.close()
            self.thread.join()

    #----------------------------------------------------------------------
    def onMessage(self, ws, evt):
        """信息推送""" 
        print evt
        
    #----------------------------------------------------------------------
    def onError(self, ws, evt):
        """错误推送"""
        print 'onError'
        print evt
        
    #----------------------------------------------------------------------
    def onClose(self, ws):
        """接口断开"""
        print 'onClose'
        
    #----------------------------------------------------------------------
    def onOpen(self, ws):
        """接口打开"""
        print 'onOpen'
    
    #----------------------------------------------------------------------
    def __fill(self, value, lenght, fillByte):
        if len(value) >= lenght:
            return value
        else:
            fillSize = lenght - len(value)
        return value + chr(fillByte) * fillSize
    #----------------------------------------------------------------------
    def __doXOr(self, s, value):
        slist = list(s)
        for index in xrange(len(slist)):
            slist[index] = chr(ord(slist[index]) ^ value)
        return "".join(slist)
    #----------------------------------------------------------------------
    def __hmacSign(self, aValue, aKey):
        keyb   = struct.pack("%ds" % len(aKey), aKey)
        value  = struct.pack("%ds" % len(aValue), aValue)
        k_ipad = self.__doXOr(keyb, 0x36)
        k_opad = self.__doXOr(keyb, 0x5c)
        k_ipad = self.__fill(k_ipad, 64, 54)
        k_opad = self.__fill(k_opad, 64, 92)
        m = hashlib.md5()
        m.update(k_ipad)
        m.update(value)
        dg = m.digest()
        
        m = hashlib.md5()
        m.update(k_opad)
        subStr = dg[0:16]
        m.update(subStr)
        dg = m.hexdigest()
        return dg

    #----------------------------------------------------------------------
    def __digest(self, aValue):
        value  = struct.pack("%ds" % len(aValue), aValue)
        h = sha.new()
        h.update(value)
        dg = h.hexdigest()
        return dg

    #----------------------------------------------------------------------
    def generateSign(self, params):
        #参数按照ASCII值排序: {"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","amount":"100","channel":"borrow","coin":"qc","event":"addChannel","interestRateOfDay":"0.15","isLoop":"1","no":"test001","repaymentDay":"20","safePwd":"123456"}
        #secretKey 加密后:86429c69799d3d6ac5da5c2c514baa874d75a4ba
        SHA_secret = self.__digest(self.secretKey)
        #计算出sign: 378348551685d6d5ddbdf02a4df71f43
        return self.__hmacSign( paramsStr, SHA_secret)

    #----------------------------------------------------------------------
    def borrow(self, symbol_pair, type_, price, amount):
        json = "{"accesskey":"ce2a18e0-dshs-4c44-4515-9aca67dd706e","amount":"100","channel":"borrow","coin":"qc","event":"addChannel","interestRateOfDay":"0.15","isLoop":"1","no":"test001","repaymentDay":"20","safePwd":"123456","sign":"378348551685d6d5ddbdf02a4df71f43"}";
        try:
            self.ws_sub_spot.send(json)
        except websocket.WebSocketConnectionClosedException:
            pass 
```