# WebSocket API
ZB为用户提供了一个简单的而又强大的API，旨在帮助用户快速高效的将ZB交易功能整合到自己应用当中。

# WebSocket服务地址
ZB WebSocket服务连接地址：wss://api.zb.cn:9999/websocket

# 示例代码
如果在使用过程中有任何问题请联系我们API技术QQ群： 764113552，我们将在第一时间帮您解决技术问题。

签名方式： 先用sha加密secretkey，然后根据加密过的secretkey把请求的参数签名，请求参数按照ascii值排序加密，通过md5填充16位加密

[github 示例代码](https://github.com/zb2017/api?_blank)

# 访问限制
 1.单个用户限制每秒钟30次访问，一秒钟内30次以上的请求，将会视作无效。
