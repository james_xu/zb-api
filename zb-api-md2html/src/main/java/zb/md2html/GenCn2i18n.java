package zb.md2html;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.alibaba.fastjson.JSONObject;

import cn.hutool.core.util.ReUtil;
import jodd.io.FileUtil;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.val;
import zb.md2html.entity.I18n;

/**
 * 针对html内容提取出所有中文并生成翻译文件(不对原来翻译文件进行覆盖,需要手动修改)
 */
@RequiredArgsConstructor
public class GenCn2i18n {
	private @NonNull String projectPath;
	/** 要提取出中文的页面 */
	private @NonNull String html;

	private final String pattern = "([\\u4e00-\\u9fa5]+)";
	public String gen() throws IOException {
		val resultFindAll = ReUtil.findAll(pattern, html, 0, new ArrayList<String>());

		val map = new HashMap<String, I18n>();
		resultFindAll.forEach(str -> {
			I18n i18nVal = new I18n(str);
			i18nVal.setEn("");
			map.put(str, i18nVal);
		});
		val json = JSONObject.toJSONString(map);
		val file = projectPath + "server\\langs\\api.server.lang.json_代码生成的翻译文件";
		FileUtil.writeString(file, json);
		System.out.println("生成翻译文件:" + file);
		return ReUtil.replaceAll(html, pattern, "<%=LANG('$1')%>");
	}
}
