package zb.md2html.gen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import com.jfinal.kit.PathKit;

import jodd.util.StringUtil;
import jodd.util.Wildcard;
import kits.my.BufferKit;
import lombok.val;
import zb.md2html.entity.Language;
import zb.md2html.entity.Table;
import zb.md2html.entity.Table.Line;
import zb.md2html.type.MdPreType;
import zb.md2html.type.MdTableType;

public abstract class ZbBase {
	// tr
	protected final String trRequest = "<tr><th>${param1}</th><th>${param2}</th><th>${param3}</th></tr>";
	protected final String trResponse = "<tr><th>{}</th><th>{}</th></tr>";
	// td
	protected final String tdRequest = "<tr><td>${param1}</td><td>${param2}</td><td>${param3}</td></tr>";
	protected final String tdResponse = "<tr><td>{}</td><td>{}</td></tr>";

	protected final String ntTemp = "<span class='nt'>{}</span><span class='s2'>:{}</span>";
	protected final String pTemp = "<span class='nt'>{}</span>";
	protected final String kong = "<span class='w'> </span>";
	protected final String h1Temp = "<h1 id='{}'>{}</h1>";
	protected final String lanTemp = "<a href='#' data-language-name='{}'>{}</a>";
	//代码块注释模板
	protected final String zhushiTemp = "<span class='c1'>{}</span>";
	/**临时文件(用于string转List<String>)*/
	protected final String tempFile = PathKit.getRootClassPath() + "/template/temp";
	protected final String fileH2 = PathKit.getRootClassPath() + "/template/h2.template";
	protected final String tableTemplate = PathKit.getRootClassPath() + "/template/table.template";
	protected final String fileResponse = PathKit.getRootClassPath() + "/template/response.template";
	protected final String response_noLan = PathKit.getRootClassPath() + "/template/response-noLan.template";
	
	protected final String allJs = PathKit.getRootClassPath() + "/template/all.js.template";
	protected final String afterTemplate = PathKit.getRootClassPath() + "/template/after.template";
	
	protected final String switchBody = PathKit.getRootClassPath() + "/template/switch-body.template";
	protected final String switchWrapper = PathKit.getRootClassPath() + "/template/switch-wrapper.template";
	

	protected final String fileApi = PathKit.getRootClassPath() + "/api/v1/";// 精确到pi目录,下面有rest跟ws目录
	
	/**代码关键字高亮*/
	protected final String height = "<span class='kd'>$0</span>";
	/** 需要高亮的关键字 */
	protected final List<String> highlight = Arrays.asList("out", "println", "Arrays", "contains", "for", "List",
			"throws", "System", "else", "if", "var", "php", "funcation", "import", "require", "def", "end",
			"public", "void", "static", "private", "return");
	/**要生成的代码块语言*/
	protected final List<MdPreType> languages = Arrays.asList(MdPreType.request,MdPreType.java,MdPreType.python);
	
	/**
	 * 获取语言代码块模板
	 * @return
	 */
	public Language getLans() {
		//curl","java
		val lans = new HashSet<String>();
		BufferKit sb = new BufferKit();
		languages.forEach(lan->{
			val name = lan.name();
			lans.add(name);
			sb.append(lanTemp,name.toLowerCase(),name.toUpperCase());
		});
		return new Language(lans, sb.toString());
	}

	public Table getMdTable(MdTableType mdType, List<String> mdLines) {
		boolean run = false;
		int i = 0;
		Table table = new Table();
		List<Line> entitys = new ArrayList<>();
		table.setTds(entitys);
		for (String line : mdLines) {
			if (line.isEmpty()) {// end
				i += 1;
			}
			String[] splits = line.split("\\|");

			if (i == 2) {
				run = false;
			}
			if (run) {
				String key = splits[0];
				if (StringUtil.isBlank(line) || "---".equals(key)) {
					continue;
				}
				String type = line.split("\\|")[1];
				String remark = splits.length == 3 ? line.split("\\|")[2] : null;
				Line entity = new Line(key, type, remark);
				if ("参数名称 ".equals(key)) {// 标题
					table.setTr(entity);
				} else {
					entitys.add(entity);
				}
			}
			if (Wildcard.matchPath(line, "*" + mdType.name() + "*")) {// start
				run = true;
			}
		}
		return table;
	}
}
