package indicator.calculate;

import java.io.Serializable;
import java.util.List;

import entity.exchange.KlineResult;
import lombok.AllArgsConstructor;
import lombok.Data;

public class Ma implements Serializable {
	/**
	 * MaEntity
	 */
	private static final long serialVersionUID = 8015287739111836368L;

	private List<KlineResult> m_kData = null;

	private int m_iParam[] = { 5, 10, 20, 30 };
	private double m_data[][];

	public Ma(List<KlineResult> kData) {
		m_kData = kData;
	}
	
	public Ma(List<KlineResult> kData,int[] m_iParam) {
		m_kData = kData;
		this.m_iParam = m_iParam;
	}
	

	public MaEntity getMa() {
		m_data = new double[m_iParam.length][];
		if (m_kData == null || m_kData.size() == 0) {
			return null;
		}
		MaEntity maEntity = new MaEntity();
		m_data[0] = new double[m_kData.size()];
		maEntity.setMa5(new IBase(m_kData).averageClose(m_iParam[0]));
		maEntity.setMa10(new IBase(m_kData).averageClose(m_iParam[1]));
		maEntity.setMa20(new IBase(m_kData).averageClose(m_iParam[2]));
		maEntity.setMa30(new IBase(m_kData).averageClose(m_iParam[3]));
		return maEntity;
	}
	
	@Data
	public static class MaEntity implements Serializable {
		/**
		 * maEntity
		 */
		private static final long serialVersionUID = 3992550363728972752L;
		private double[] ma5;
		private double[] ma10;
		private double[] ma20;
		private double[] ma30;
		
		@Data
		@AllArgsConstructor
		public static class MaOne implements Serializable{
			private static final long serialVersionUID = 944736376415330951L;
			private double ma5;
			private double ma10;
			private double ma20;
			private double ma30;
			
			@Override
			public String toString() {
				return "均 ma5:" + ma5 + " ma10:" + ma10+ " ma20:"
						+ ma20 + " ma30:" + ma30;
			}
		}
		
		public MaOne getOne(int i){
			MaOne maOne = new MaOne(ma5[i],ma10[i],ma20[i],ma30[i]);
			return maOne;
		}
		
		public MaOne getLast(){
			int last = ma5.length - 1;
			MaOne maOne = new MaOne(ma5[last],ma10[last],ma20[last],ma30[last]);
			return maOne;
		}
	}

}
