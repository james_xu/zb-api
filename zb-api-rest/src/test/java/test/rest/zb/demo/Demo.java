package test.rest.zb.demo;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import entity.commons.UserApi;
import entity.enumtype.ExchangeEnum;
import jodd.props.Props;
import jodd.util.Wildcard;
import rest.zb.rest.api.RestApiGet;
import rest.zb.rest.api.RestApiPost;
import rest.zb.rest.entity.Ti;
import rest.zb.rest.entity.TradeResult;
import rest.zb.rest.entity.post.Account;

public class Demo {
	private RestApiPost apiPost;
	private RestApiGet apiGet;
	
	
	
	@Test
	public void 子账户() {
		String leverAssetsInfo = apiPost.getLeverAssetsInfo();
		System.out.println(leverAssetsInfo);
//		apiPost.addSubUser();
		
		//{"code":1000,"message":"操作成功","result":[{"isOpenApi":false,"memo":"dfdfd","pwdLevel":0,"userName":"13415000099@test12","userId":358675,"isFreez":false}]}
//		apiPost.getSubUserList();
		
		
//		{"code":1000,"message":"操作成功","result":{"apiSecret":"131a3bf1-6bc6-4921-a766-e9ab5e55d046","apiKey":"773e4fe6-26dc-4816-b25a-f45ab03b43b7"}}
//		apiPost.createSubUserKey();
		
//		apiPost.doTransferFunds();
		
//		apiPost.getAccountTemp();
	}

//	@Test
	public void 行情api() {
//		System.out.println("dfdf");
		
//		this.getMarkets();
		
		Map<String, Ti> allTicker = this.getAllTicker();
		System.out.println(allTicker.keySet());
		allTicker.forEach((symbol,t)->{
//			if(Wildcard.matchPath(symbol, "*zb")) {
//				System.out.println(symbol);	
//			}
			
			if(Wildcard.matchPath(symbol, "btd*")) {
				System.out.println(symbol);	
			}
		});
		
		
		//比如我要获取zbqc这个交易对
//		Ti ti = allTicker.get("zbqc");
//		System.out.println(ti);

		// 获取k线图
//		List<KlineResult> kline = apiGet.getKline(KTimeEnum.min1);
//		KlineResult klineResult = kline.get(kline.size() - 1);
//		System.out.println(klineResult.getDate()+" "+klineResult);

//		获取买卖方所有深度
//		DeptResult dept = apiGet.getDept();
//		// 获取买方深度
//		List<Dept> asks = dept.getAsks();
//		// 卖1详情
//		Dept ask1 = asks.get(0);
//		System.out.println("卖1,价:" + ask1.getPrice() + ",量:" + ask1.getAmount());
//		List<Dept> bids = dept.getBids();
//		Dept bid1 = bids.get(0);
//		System.out.println("买1,价:" + bid1.getPrice() + ",量:" + bid1.getAmount());
	}
	
	private void getMarkets() {
		String json = apiGet.getMarkets();
		JSONArray parseArray = JSONObject.parseArray(json);
		parseArray.forEach(a->{
			System.out.println(a);
		});
	}
	
	public Map<String,Ti> getAllTicker() {
		String allTicker = apiGet.getAllTicker();
		System.out.println(allTicker);
		JSONObject marketsObj = JSONObject.parseObject(allTicker);
		Set<String> keySet = marketsObj.keySet();
		Map<String,Ti> map = new HashMap<String,Ti>();
		keySet.forEach(symbol->{
			Ti parseObject = JSONObject.parseObject(marketsObj.getString(symbol), Ti.class);
			map.put(symbol, parseObject);
		});
		return map;
	}

	@Test
	public void 交易api() {
//		System.out.println("行情api针对的交易对:" + apiPost.getSymbol());
//		// 挂单
//		double price = apiGet.getDept().getBids().get(2).getPrice();// 获取买4价格挂单
		TradeResult buyResult = apiPost.buy(0.4, 1);
		long id = buyResult.getId();// 返回挂单id
		System.out.println("下单返回结果:" + buyResult + ",挂单Id:" + id);
//		// 取消订单
//		TradeResult cancelResult = apiPost.cancelOrder(id);
//		System.out.println("取消订单结果:" + cancelResult);
//		// 查询订单id详情
//		Order order = apiPost.getOrder(id);
//		System.out.println("订单id:" + order.getId() + ",详情:" + order);
//		// 查询未成交订单
//		List<Order> orders = apiPost.getUnfinishedOrdersIgnoreTradeType(1);
//		orders.forEach(o -> {
//			System.out.println("未成交订单:" + o);
//		});

		// 遍历用户信息
//		Account account = apiPost.getAccount();
//		account.getResult().getCoins().forEach(coin -> {
//			System.out.println("可用:" + coin.getAvailable() + ",冻结:" + coin.getFreez());
//		});
	}
	
//	@Test
	public void 提币() {
		String withdraw = apiPost.withdraw(2, 1, "dizhi1232132dfd");
		System.out.println(withdraw);
	}

	@Before
	public void init() throws IOException {
		String symbol = "brc_usdt";
		String url_base = "http://api.zb.cn/data/v1";
		// 构造行情api对象
		apiGet = new RestApiGet(url_base, symbol);

		Props p = new Props();
		p.load(new File("c:/config/zb.txt"));
		String apiKey = p.getValue("user.zb.apikey");// 修改为自己的公钥
//		apiKey = "54a0e532-f3b7-47d5-97c9-d210b37e9958";
		String secretKey = p.getValue("user.zb.secretKey");// 修改为自己的私钥
//		secretKey = "927cb8d2-1740-40aa-8124-f2f187082553";
		String url = "https://trade.zb.cn/api/";
		// 构造交易接口对象
		UserApi userApi = new UserApi(ExchangeEnum.zb, "测试", apiKey, secretKey);
		userApi.setPayPass("123456bb");
		apiPost = new RestApiPost(url, symbol, userApi);

	}
}
